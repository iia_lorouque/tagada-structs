# frozen_string_literal: true

require_relative 'common'
require_relative 'specification'

module Differential

  class Hundredth < DataClass
    attribute :value, Types::Strict::Integer

    def to_json(*options)
      value.to_json(*options)
    end

    def self.from_json(value)
      Hundredth.new(value: value)
    end

    def to_s
      "%.2f" % (value / 100.0)
    end
  end

  class MinusLog2Probability < DataClass
    attribute :hundredth, Hundredth

    def to_json(*options)
      hundredth.to_json(*options)
    end

    def self.from_json(hundredth)
      MinusLog2Probability.new(hundredth: Hundredth.from_json(hundredth))
    end

    def to_s
      "2^{-#{hundredth}}"
    end
  end

  class DifferentialStateNode < SealedClass
    sub_classes "Variable", "Constant", "Probability"
  end

  class Variable < DifferentialStateNode
    sealed!
    attribute :domain, Domain
  end

  class Probability < DifferentialStateNode
    sealed!
    attribute :domain, Types::Strict::Array.of(MinusLog2Probability)
  end

  class Constant < DifferentialStateNode
    sealed!
    attribute :value, Types::Strict::Integer
  end

  class DifferentialFunction < SealedClass
    sub_classes "DDT", "Linear"
  end

  class DDTArgs < DataClass
    attribute :delta_in, Types::Strict::Integer
    attribute :delta_out, Types::Strict::Integer

    def to_json(*options)
      [delta_in, delta_out].to_json(options)
    end

    def self.from_json(fields)
      DDTArgs.new(delta_in: fields[0], delta_out: fields[1])
    end
  end

  class DDT < DifferentialFunction
    sealed!
    attribute :probabilities, Types::Strict::Hash.map(DDTArgs, MinusLog2Probability)
  end

  class Linear < DifferentialFunction
    sealed!
    attribute :linear, Specification::LinearFunction
  end

  class DifferentialFunctionNode < DataClass
    attribute :domain, Types::Strict::Array.of(Domain)
    attribute :co_domain, Types::Strict::Array.of(Domain)
    attribute :function, DifferentialFunction
  end

  class DifferentialStateNodeUID < DataClass
    attribute :uid, Types::Strict::Integer

    def to_json(*options)
      uid.to_json(*options)
    end

    def self.from_json(uid)
      DifferentialStateNodeUID.new(uid: uid)
    end
  end

  class DifferentialFunctionNodeUID < DataClass
    attribute :uid, Types::Strict::Integer

    def to_json(*options)
      uid.to_json(*options)
    end

    def self.from_json(uid)
      DifferentialStateNodeUID.new(uid: uid)
    end
  end

  class DifferentialTransition < DataClass
    attribute :inputs, Types::Strict::Array.of(DifferentialStateNodeUID)
    attribute :function, DifferentialFunctionNodeUID
    attribute :outputs, Types::Strict::Array.of(DifferentialStateNodeUID)
  end

  class DifferentialGraph < DataClass
    attribute :version, Version
    attribute :states, Types::Strict::Array.of(DifferentialStateNode)
    attribute :functions, Types::Strict::Array.of(DifferentialFunctionNode)
    attribute :transitions, Types::Strict::Array.of(DifferentialTransition)
    attribute :plaintext, Types::Strict::Array.of(DifferentialStateNodeUID)
    attribute :key, Types::Strict::Array.of(DifferentialStateNodeUID)
    attribute :ciphertext, Types::Strict::Array.of(DifferentialStateNodeUID)
    attribute :names, Types::Strict::Hash.map(Types::Strict::String, DifferentialStateNodeUID)
    attribute :blocks, Types::Strict::Hash.map(Types::Strict::String, Types::Strict::Array.of(DifferentialStateNodeUID))
  end
end