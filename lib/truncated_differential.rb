# frozen_string_literal: true

require_relative 'common'
require_relative 'specification'

module TruncatedDifferential

  class TruncatedDifferentialStateNode < SealedClass
    sub_classes "Variable", "Constant"
  end

  class Variable < TruncatedDifferentialStateNode
    sealed!
    attribute :domain, Domain
  end

  class Constant < TruncatedDifferentialStateNode
    sealed!
    attribute :value, Types::Strict::Integer
  end

  class TruncatedDifferentialFunction < SealedClass
    sub_classes "NonLinearTransition", "Linear"
  end

  class NonLinearTransition < TruncatedDifferentialFunction
    sealed!
    attribute :maximum_activation_probability, Differential::MinusLog2Probability
  end

  class Linear < TruncatedDifferentialFunction
    sealed!
    attribute :truth_table, Types::Strict::Array.of(Types::Strict::Array.of(Types::Strict::Bool))
  end

  class TruncatedDifferentialFunctionNode < DataClass
    attribute :domain, Types::Strict::Array.of(Domain)
    attribute :co_domain, Types::Strict::Array.of(Domain)
    attribute :function, TruncatedDifferentialFunction
  end

  class TruncatedDifferentialStateNodeUID < DataClass
    attribute :uid, Types::Strict::Integer

    def to_json(*options)
      uid.to_json(*options)
    end

    def self.from_json(uid)
      TruncatedDifferentialStateNodeUID.new(uid: uid)
    end
  end

  class TruncatedDifferentialFunctionNodeUID < DataClass
    attribute :uid, Types::Strict::Integer

    def to_json(*options)
      uid.to_json(*options)
    end

    def self.from_json(uid)
      TruncatedDifferentialStateNodeUID.new(uid: uid)
    end
  end

  class TruncatedDifferentialTransition < DataClass
    attribute :inputs, Types::Strict::Array.of(TruncatedDifferentialStateNodeUID)
    attribute :function, TruncatedDifferentialFunctionNodeUID
    attribute :outputs, Types::Strict::Array.of(TruncatedDifferentialStateNodeUID)
  end

  class TruncatedDifferentialGraph < DataClass
    attribute :version, Version
    attribute :states, Types::Strict::Array.of(TruncatedDifferentialStateNode)
    attribute :functions, Types::Strict::Array.of(TruncatedDifferentialFunctionNode)
    attribute :transitions, Types::Strict::Array.of(TruncatedDifferentialTransition)
    attribute :plaintext, Types::Strict::Array.of(TruncatedDifferentialStateNodeUID)
    attribute :key, Types::Strict::Array.of(TruncatedDifferentialStateNodeUID)
    attribute :ciphertext, Types::Strict::Array.of(TruncatedDifferentialStateNodeUID)
    attribute :names, Types::Strict::Hash.map(Types::Strict::String, TruncatedDifferentialStateNodeUID)
    attribute :blocks, Types::Strict::Hash.map(Types::Strict::String, Types::Strict::Array.of(TruncatedDifferentialStateNodeUID))
  end
end