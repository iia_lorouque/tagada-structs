# frozen_string_literal: true

require_relative 'common'
module Specification

  ShiftLeft = "ShiftLeft"
  ShiftRight = "ShiftRight"

  class StateNode < SealedClass
    sub_classes "Variable", "Constant"
  end

  class Variable < StateNode
    attribute :type, Types::Strict::String.default("Variable").enum("Variable")
    attribute :domain, Domain
  end

  class Constant < StateNode
    attribute :type, Types::Strict::String.default("Constant").enum("Constant")
    attribute :value, Types::Strict::Integer
  end

  class Function < SealedClass
    sub_classes "SBox", "Linear"
  end

  class SBox < Function
    sealed!
    attribute :lookup_table, Types::Strict::Array.of(Types::Strict::Integer)
  end

  class LinearFunction < SealedClass
    sub_classes "Equal", "BitwiseXOR", "Table", "Command", "GFMul", "GFMatToVecMul", "LFSR", "Split", "Concat", "Shift", "CircularShift", "AndConst", "OrConst"
  end

  class Equal < LinearFunction
    sealed!
  end

  class BitwiseXOR < LinearFunction
    sealed!
  end

  class Table < LinearFunction
    sealed!
    attribute :tuples, Types::Strict::Array.of(Types::Strict::Array.of(Types::Strict::Integer))
  end

  class Command < LinearFunction
    sealed!
    attribute :executable, Types::Strict::String
  end

  class GFMul < LinearFunction
    sealed!
    attribute :poly, Types::Strict::Integer
    attribute :constant, Types::Strict::Integer
  end

  class GFMatToVecMul < LinearFunction
    sealed!
    attribute :poly, Types::Strict::Integer
    attribute :m, Types::Strict::Array.of(Types::Strict::Array.of(Types::Strict::Integer))
  end

  class LFSR < LinearFunction
    sealed!
    attribute :nb_bits, Types::Strict::Integer
    attribute :poly, Types::Strict::Integer
    attribute :direction, Types::Strict::String.enum("ShiftLeft", "ShiftRight")
  end

  class Split < LinearFunction
    sealed!
    attribute :to_words, Types::Strict::Array.of(Types::Strict::Array.of(Types::Strict::Integer))
  end

  class BitAndWord < DataClass
    attribute :bit_pos, Types::Strict::Integer
    attribute :word_pos, Types::Strict::Integer

    def to_json(*options)
      [bit_pos, word_pos].to_json(*options)
    end

    def self.from_json(fields)
      BitAndWord.new(bit_pos: fields[0], word_pos: fields[1])
    end
  end

  class Concat < LinearFunction
    sealed!
    attribute :to_word, Types::Strict::Array.of(BitAndWord)
  end

  class Shift < LinearFunction
    sealed!
    attribute :nb_bits, Types::Strict::Integer
    attribute :bit_count, Types::Strict::Integer
    attribute :direction, Types::Strict::String.enum("ShiftLeft", "ShiftRight")
  end

  class CircularShift < LinearFunction
      sealed!
      attribute :nb_bits, Types::Strict::Integer
      attribute :bit_count, Types::Strict::Integer
      attribute :direction, Types::Strict::String.enum("ShiftLeft", "ShiftRight")
  end

  class AndConst < LinearFunction
    sealed!
    attribute :constant, Types::Strict::Integer
  end

  class OrConst < LinearFunction
    sealed!
    attribute :constant, Types::Strict::Integer
  end

  class Linear < Function
    sealed!
    attribute :linear, LinearFunction
  end

  class FunctionNode < DataClass
    attribute :domain, Types::Strict::Array.of(Domain)
    attribute :co_domain, Types::Strict::Array.of(Domain)
    attribute :function, Function
  end

  class StateNodeUID < DataClass
    attribute :uid, Types::Strict::Integer

    def to_json(*options)
      uid.to_json(*options)
    end

    def self.from_json(uid)
      StateNodeUID.new(uid: uid)
    end
  end

  class FunctionNodeUID < DataClass
    attribute :uid, Types::Strict::Integer

    def to_json(*options)
      uid.to_json(*options)
    end

    def self.from_json(uid)
      FunctionNodeUID.new(uid: uid)
    end
  end

  class Transition < DataClass
    attribute :inputs, Types::Strict::Array.of(StateNodeUID)
    attribute :function, FunctionNodeUID
    attribute :outputs, Types::Strict::Array.of(StateNodeUID)
  end

  class SpecificationGraph < DataClass
    attribute :version, Version
    attribute :states, Types::Strict::Array.of(StateNode)
    attribute :functions, Types::Strict::Array.of(FunctionNode)
    attribute :transitions, Types::Strict::Array.of(Transition)
    attribute :plaintext, Types::Strict::Array.of(StateNodeUID)
    attribute :key, Types::Strict::Array.of(StateNodeUID)
    attribute :ciphertext, Types::Strict::Array.of(StateNodeUID)
    attribute :names, Types::Strict::Hash.map(Types::Strict::String, StateNodeUID)
    attribute :blocks, Types::Strict::Hash.map(Types::Strict::String, Types::Strict::Array.of(StateNodeUID))
  end
end