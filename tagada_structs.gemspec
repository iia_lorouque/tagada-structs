Gem::Specification.new do |s|
  s.name        = "tagada_structs"
  s.version     = "1.1.0"
  s.summary     = "Tagada data structures for Ruby"
  s.description = "Data structures to represent ciphers as bipartite graphs"
  s.authors     = ["Loïc Rouquette"]
  s.email       = "publications@loicrouquette.fr"
  s.files       = %w[lib/common.rb lib/specification.rb lib/differential.rb lib/truncated_differential.rb lib/lib.rb sig/tagada_structs.rbs]
  s.homepage    =
    "https://gitlab.limos.fr/iia_lorouque/tagada-structs"
  s.license       = "MIT"
  s.add_development_dependency 'rbs'
  s.add_development_dependency 'dry-types'
  s.add_development_dependency 'dry-struct'
end
