package fr.limos.gitlab.decrypt.tagada.structs.differential

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.ser.std.StdSerializer

/**
 * A function node UID
 */
@JsonSerialize(using = DifferentialFunctionUID.Serializer::class)
@JsonDeserialize(using = DifferentialFunctionUID.Deserializer::class)
data class DifferentialFunctionUID(val uid: Int) {
    class Serializer(t: Class<DifferentialFunctionUID>? = null): StdSerializer<DifferentialFunctionUID>(t) {
        override fun serialize(value: DifferentialFunctionUID, gen: JsonGenerator, provider: SerializerProvider) =
            gen.writeNumber(value.uid)

    }

    class Deserializer(c: Class<*>? = null): StdDeserializer<DifferentialFunctionUID>(c) {
        override fun deserialize(p: JsonParser, ctxt: DeserializationContext) =
            DifferentialFunctionUID(p.intValue)
    }
}

