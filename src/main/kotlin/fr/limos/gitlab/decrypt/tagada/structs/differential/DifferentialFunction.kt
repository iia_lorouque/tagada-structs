package fr.limos.gitlab.decrypt.tagada.structs.differential

import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import fr.limos.gitlab.decrypt.tagada.structs.specification.LinearFunction


@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
sealed class DifferentialFunction

data class DDT(
    @JsonSerialize(keyUsing = DDTArgs.KeySerializer::class)
    @JsonDeserialize(keyUsing = DDTArgs.KeyDeserializer::class)
    val probabilities: Map<DDTArgs, MinusLog2Probability>
): DifferentialFunction()

data class Linear(val linear: LinearFunction): DifferentialFunction()