package fr.limos.gitlab.decrypt.tagada.structs.truncateddifferential

data class TruncatedDifferentialTransition(
    val inputs: List<TruncatedDifferentialStateNodeUID>,
    val function: TruncatedDifferentialFunctionNodeUID,
    val outputs: List<TruncatedDifferentialStateNodeUID>
)