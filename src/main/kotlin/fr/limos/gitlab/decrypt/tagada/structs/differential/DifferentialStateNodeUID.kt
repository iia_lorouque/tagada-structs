package fr.limos.gitlab.decrypt.tagada.structs.differential

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.ser.std.StdSerializer

/**
 * A function node UID
 */
@JsonSerialize(using = DifferentialStateNodeUID.Serializer::class)
@JsonDeserialize(using = DifferentialStateNodeUID.Deserializer::class)
data class DifferentialStateNodeUID(val uid: Int) {
    class Serializer(t: Class<DifferentialStateNodeUID>? = null): StdSerializer<DifferentialStateNodeUID>(t) {
        override fun serialize(value: DifferentialStateNodeUID, gen: JsonGenerator, provider: SerializerProvider) =
            gen.writeNumber(value.uid)
    }

    class Deserializer(c: Class<*>? = null): StdDeserializer<DifferentialStateNodeUID>(c) {
        override fun deserialize(p: JsonParser, ctxt: DeserializationContext) =
            DifferentialStateNodeUID(p.intValue)
    }

}

