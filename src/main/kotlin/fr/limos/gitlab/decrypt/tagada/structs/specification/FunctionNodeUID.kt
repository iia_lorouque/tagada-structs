package fr.limos.gitlab.decrypt.tagada.structs.specification

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.ser.std.StdSerializer

/**
 * A function node UID
 */
@JsonSerialize(using = FunctionNodeUID.Serializer::class)
@JsonDeserialize(using = FunctionNodeUID.Deserializer::class)
data class FunctionNodeUID(val uid: Int) {
    class Serializer(t: Class<FunctionNodeUID>? = null): StdSerializer<FunctionNodeUID>(t) {
        override fun serialize(value: FunctionNodeUID, gen: JsonGenerator, provider: SerializerProvider) =
            gen.writeNumber(value.uid)

    }

    class Deserializer(c: Class<*>? = null): StdDeserializer<FunctionNodeUID>(c) {
        override fun deserialize(p: JsonParser, ctxt: DeserializationContext) =
            FunctionNodeUID(p.intValue)
    }
}

