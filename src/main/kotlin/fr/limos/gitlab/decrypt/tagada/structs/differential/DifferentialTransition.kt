package fr.limos.gitlab.decrypt.tagada.structs.differential

/**
 * Represents a function call in the cipher in the form of function(inputs) = outputs
 */
data class DifferentialTransition(
    val inputs: List<DifferentialStateNodeUID>,
    val function: DifferentialFunctionUID,
    val outputs: List<DifferentialStateNodeUID>
)