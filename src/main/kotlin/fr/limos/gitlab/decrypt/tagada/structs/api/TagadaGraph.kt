package fr.limos.gitlab.decrypt.tagada.structs.api

import com.fasterxml.jackson.annotation.JsonTypeInfo
import fr.limos.gitlab.decrypt.tagada.structs.differential.DifferentialGraph
import fr.limos.gitlab.decrypt.tagada.structs.specification.SpecificationGraph
import fr.limos.gitlab.decrypt.tagada.structs.truncateddifferential.TruncatedDifferentialGraph


@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
sealed class TagadaGraph
data class Specification(val graph: SpecificationGraph): TagadaGraph()
data class Differential(val graph: DifferentialGraph): TagadaGraph()
data class Truncated(val graph: TruncatedDifferentialGraph): TagadaGraph()
