package fr.limos.gitlab.decrypt.tagada.structs.specification

import com.fasterxml.jackson.annotation.JsonFormat

@JsonFormat(shape = JsonFormat.Shape.ARRAY)
data class BitAndWord(val bitPos: Int, val wordPos: Int)