package fr.limos.gitlab.decrypt.tagada.structs.truncateddifferential

import com.fasterxml.jackson.annotation.JsonProperty
import fr.limos.gitlab.decrypt.tagada.structs.common.Domain

data class TruncatedDifferentialFunctionNode(
    val domain: List<Domain>,
    @JsonProperty("co_domain") val coDomain: List<Domain>,
    val function: TruncatedDifferentialFunction
)