package fr.limos.gitlab.decrypt.tagada.structs.truncateddifferential

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import fr.limos.gitlab.decrypt.tagada.structs.specification.StateNodeUID

@JsonSerialize(using = TruncatedDifferentialFunctionNodeUID.Serializer::class)
@JsonDeserialize(using = TruncatedDifferentialFunctionNodeUID.Deserializer::class)
data class TruncatedDifferentialFunctionNodeUID(val uid: Int) {
    class Serializer(t: Class<TruncatedDifferentialFunctionNodeUID>? = null): StdSerializer<TruncatedDifferentialFunctionNodeUID>(t) {
        override fun serialize(value: TruncatedDifferentialFunctionNodeUID, gen: JsonGenerator, provider: SerializerProvider) =
            gen.writeNumber(value.uid)

    }

    class Deserializer(c: Class<*>? = null): StdDeserializer<TruncatedDifferentialFunctionNodeUID>(c) {
        override fun deserialize(p: JsonParser, ctxt: DeserializationContext) =
            TruncatedDifferentialFunctionNodeUID(p.intValue)
    }
}