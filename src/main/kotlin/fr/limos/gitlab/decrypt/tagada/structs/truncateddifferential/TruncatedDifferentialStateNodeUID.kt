package fr.limos.gitlab.decrypt.tagada.structs.truncateddifferential

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import fr.limos.gitlab.decrypt.tagada.structs.specification.StateNodeUID

@JsonSerialize(using = TruncatedDifferentialStateNodeUID.Serializer::class)
@JsonDeserialize(using = TruncatedDifferentialStateNodeUID.Deserializer::class)
data class TruncatedDifferentialStateNodeUID(val uid: Int) {
    class Serializer(t: Class<TruncatedDifferentialStateNodeUID>? = null): StdSerializer<TruncatedDifferentialStateNodeUID>(t) {
        override fun serialize(value: TruncatedDifferentialStateNodeUID, gen: JsonGenerator, provider: SerializerProvider) =
            gen.writeNumber(value.uid)
    }

    class Deserializer(c: Class<*>? = null): StdDeserializer<TruncatedDifferentialStateNodeUID>(c) {
        override fun deserialize(p: JsonParser, ctxt: DeserializationContext) =
            TruncatedDifferentialStateNodeUID(p.intValue)
    }
}