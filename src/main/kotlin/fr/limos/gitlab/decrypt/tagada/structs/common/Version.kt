package fr.limos.gitlab.decrypt.tagada.structs.common

import com.fasterxml.jackson.annotation.JsonFormat

/**
 * Represents an API version in the format MAJOR.MINOR.PATCH
 */
@JsonFormat(shape = JsonFormat.Shape.ARRAY)
data class Version(val major: Int, val minor: Int, val patch: Int)
