package fr.limos.gitlab.decrypt.tagada.structs.specification

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonTypeInfo
import fr.limos.gitlab.decrypt.tagada.structs.common.Value


@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
sealed class Function

data class SBox(@JsonProperty("lookup_table") val lookupTable: List<Value>): Function()

data class Linear(val linear: LinearFunction): Function()