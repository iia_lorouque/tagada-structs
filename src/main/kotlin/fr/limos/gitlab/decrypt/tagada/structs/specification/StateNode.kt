package fr.limos.gitlab.decrypt.tagada.structs.specification

import com.fasterxml.jackson.annotation.JsonTypeInfo
import fr.limos.gitlab.decrypt.tagada.structs.common.Domain
import fr.limos.gitlab.decrypt.tagada.structs.common.Value

/**
 * Represents a state of the cipher can be either a variable or a constant.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
sealed class StateNode

/**
 * Represents a half open range [min; max[
 */
data class Variable(val domain: Domain) : StateNode()

/**
 * Represents a set of values
 */
data class Constant(val value: Value) : StateNode()
///