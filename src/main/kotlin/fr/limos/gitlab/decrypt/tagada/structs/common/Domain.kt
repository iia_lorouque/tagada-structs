package fr.limos.gitlab.decrypt.tagada.structs.common

import com.fasterxml.jackson.annotation.JsonTypeInfo

/**
 * Represents the managed Domains
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
sealed class Domain

/**
 * Represents a half open range [min; max[
 */
data class Range(val min: Int, val max: Int) : Domain()

/**
 * Represents a set of values
 */
data class Sparse(val values: Set<Value>) : Domain()
///