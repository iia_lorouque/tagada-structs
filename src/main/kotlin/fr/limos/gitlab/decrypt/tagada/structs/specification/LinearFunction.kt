package fr.limos.gitlab.decrypt.tagada.structs.specification

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonTypeInfo
import fr.limos.gitlab.decrypt.tagada.structs.common.Value

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
sealed class LinearFunction
/**
 * Represents the equal (\(=\)) operator
 */
object Equal : LinearFunction() {
    override fun equals(other: Any?) = other is Equal
    override fun hashCode() = 0b0001_0001
}

/**
 * Represents the bitwise XOR ( \( \oplus\) ) operator
 */
object BitwiseXOR : LinearFunction() {
    override fun equals(other: Any?) = other is BitwiseXOR
    override fun hashCode() = 0b0001_0010
}

/**
 * Represents a function in extension.
 * @param tuples The list of valid tuples
 */
data class Table(val tuples: List<List<Value>>) : LinearFunction()

/**
 * Represents a black box operator as an executable path. The executable must accept the function argument as inline
 * arguments, e.g. `times 2 3` and must return the values separated by a single space, e.g. `4 5 6`.
 */
data class Command(val executable: String) : LinearFunction()

/**
 * Represents a Galois Field multiplication using the given irreducible polynomial
 * @param poly the polynomial used to perform the multiplication
 */
data class GFMul(val poly: Int, val constant: Int) : LinearFunction()

/**
 * Represents a Galois Field matrix to column vector multiplication using the given irreducible polynomial
 * @param poly the polynomial used to perform the underlying galois field multiplication
 * @param m the multiplication matrix
 */
data class GFMatToVecMul(val poly: Int, val m: List<List<Value>>): LinearFunction()

/**
 * Represents a LFSR function (https://en.wikipedia.org/wiki/Linear-feedback_shift_register)
 * The function output is equal to a cyclic shift (either Left or Right) of its internal register.
 * For a left (resp. right) shift, the rightmost (resp. leftmost) bit is overridden by a XOR between the internal
 * register and a polynomial.
 * @param nbBits The number of bits of the LFSR
 * @param poly The polynomial used in the LFSR
 * @param direction The shift direction
 */
data class LFSR(@JsonProperty("nb_bits") val nbBits: Int, val poly: Int, val direction: Direction): LinearFunction()

data class Split(@JsonProperty("to_words") val toWords: List<List<Int>>): LinearFunction()
data class Concat(@JsonProperty("to_word") val toWord: List<BitAndWord>): LinearFunction()

data class Shift(@JsonProperty("nb_bits") val nbBits: Int, @JsonProperty("bit_count") val bitCount: Int, val direction: Direction): LinearFunction()
data class CircularShift(@JsonProperty("nb_bits") val nbBits: Int, @JsonProperty("bit_count") val bitCount: Int, val direction: Direction): LinearFunction()
data class AndConst(val constant: Value): LinearFunction()
data class OrConst(val constant: Value): LinearFunction()
///