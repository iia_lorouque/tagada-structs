package fr.limos.gitlab.decrypt.tagada.structs.specification

import fr.limos.gitlab.decrypt.tagada.structs.common.Version

/**
 * Represents a cipher Dag (directed acyclic graph) in Tagada.
 * @param version the library version that generates the JSON
 * @param states the states of the cipher. The states include the plaintext, the key, the ciphertext, the internal
 * states and the constants of the cipher
 * @param functions the functions used in the cipher
 * @param transitions the links between the states using one of the cipher function
 */
data class SpecificationGraph(
    val version: Version,
    val states: List<StateNode>,
    val functions: List<FunctionNode>,
    val transitions: List<Transition>,
    val plaintext: List<StateNodeUID>,
    val key: List<StateNodeUID>,
    val ciphertext: List<StateNodeUID>,
    val names: Map<String, StateNodeUID>,
    val blocks: Map<String, List<StateNodeUID>>,
)