package fr.limos.gitlab.decrypt.tagada.structs

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import java.nio.file.Files
import java.nio.file.Path

val mapper = jacksonObjectMapper()

fun readTextFrom(path: String): String {
    val file = Path.of("src/test/resources/%s.json".format(path))
    return Files.readString(file)
}

inline fun <reified T> readJsonFrom(path: String): T {
    val file = Path.of("src/test/resources/%s.json".format(path))
    return mapper.readValue(file.toFile())
}