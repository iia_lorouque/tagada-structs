package fr.limos.gitlab.decrypt.tagada.structs

import fr.limos.gitlab.decrypt.tagada.structs.common.Range
import fr.limos.gitlab.decrypt.tagada.structs.specification.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test


class Specification {
    @Test
    fun serializeVariable() {
        val variable = Variable(Range(0, 256))
        assertEquals(readTextFrom("state_node/variable"), mapper.writeValueAsString(variable))
    }

    @Test
    fun deserializeVariable() {
        val variable = Variable(
            Range(0, 256),
        )
        assertEquals(variable, readJsonFrom<StateNode>("state_node/variable"))
    }

    @Test
    fun serializeConstant() {
        val constant = Constant(
            10
        )
        assertEquals(readTextFrom("state_node/constant"), mapper.writeValueAsString(constant))
    }

    @Test
    fun deserializeConstant() {
        val constant = Constant(10)
        assertEquals(constant, readJsonFrom<StateNode>("state_node/constant"))
    }

    @Test
    fun serializeFunctionNode() {
        val byte = Range(0, 256)
        val functionNode = FunctionNode(
            listOf(byte),
            listOf(byte),
            Linear(Equal)
        )
        assertEquals(readTextFrom("function_node"), mapper.writeValueAsString(functionNode))
    }

    @Test
    fun deserializeFunctionNode() {
        val byte = Range(0, 256)
        val functionNode = FunctionNode(
            listOf(byte),
            listOf(byte),
            Linear(Equal)
        )
        assertEquals(functionNode, readJsonFrom<FunctionNode>("function_node"))
    }

    @Test
    fun serializeEqual() {
        assertEquals(readTextFrom("function/equal"), mapper.writeValueAsString(Equal))
    }

    @Test
    fun deserializeEqual() {
        assertEquals(Equal, readJsonFrom<Equal>("function/equal"))
    }

    @Test
    fun serializeBitwiseXOR() {
        assertEquals(readTextFrom("function/bitwise_xor"), mapper.writeValueAsString(BitwiseXOR))
    }

    @Test
    fun deserializeBitwiseXOR() {
        assertEquals(BitwiseXOR, readJsonFrom<BitwiseXOR>("function/bitwise_xor"))
    }

    @Test
    fun serializeSBox() {
        val sbox = SBox(
            listOf(
                0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76,
                0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0,
                0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15,
                0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75,
                0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84,
                0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf,
                0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8,
                0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2,
                0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73,
                0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb,
                0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79,
                0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08,
                0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a,
                0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e,
                0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf,
                0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16,
            )
        )
        assertEquals(readTextFrom("function/sbox"), mapper.writeValueAsString(sbox))
    }

    @Test
    fun deserializeSBox() {
        val sbox = SBox(
            listOf(
                0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76,
                0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0,
                0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15,
                0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75,
                0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84,
                0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf,
                0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8,
                0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2,
                0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73,
                0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb,
                0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79,
                0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08,
                0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a,
                0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e,
                0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf,
                0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16,
            )
        )
        assertEquals(sbox, readJsonFrom<SBox>("function/sbox"))
    }

    @Test
    fun serializeTable() {
        val nibbleXOR = Table(
            listOf(
                listOf(0, 0, 0),
                listOf(0, 1, 1),
                listOf(0, 2, 2),
                listOf(0, 3, 3),
                listOf(0, 4, 4),
                listOf(0, 5, 5),
                listOf(0, 6, 6),
                listOf(0, 7, 7),
                listOf(0, 8, 8),
                listOf(0, 9, 9),
                listOf(0, 10, 10),
                listOf(0, 11, 11),
                listOf(0, 12, 12),
                listOf(0, 13, 13),
                listOf(0, 14, 14),
                listOf(0, 15, 15),
                listOf(1, 0, 1),
                listOf(1, 1, 0),
                listOf(1, 2, 3),
                listOf(1, 3, 2),
                listOf(1, 4, 5),
                listOf(1, 5, 4),
                listOf(1, 6, 7),
                listOf(1, 7, 6),
                listOf(1, 8, 9),
                listOf(1, 9, 8),
                listOf(1, 10, 11),
                listOf(1, 11, 10),
                listOf(1, 12, 13),
                listOf(1, 13, 12),
                listOf(1, 14, 15),
                listOf(1, 15, 14),
                listOf(2, 0, 2),
                listOf(2, 1, 3),
                listOf(2, 2, 0),
                listOf(2, 3, 1),
                listOf(2, 4, 6),
                listOf(2, 5, 7),
                listOf(2, 6, 4),
                listOf(2, 7, 5),
                listOf(2, 8, 10),
                listOf(2, 9, 11),
                listOf(2, 10, 8),
                listOf(2, 11, 9),
                listOf(2, 12, 14),
                listOf(2, 13, 15),
                listOf(2, 14, 12),
                listOf(2, 15, 13),
                listOf(3, 0, 3),
                listOf(3, 1, 2),
                listOf(3, 2, 1),
                listOf(3, 3, 0),
                listOf(3, 4, 7),
                listOf(3, 5, 6),
                listOf(3, 6, 5),
                listOf(3, 7, 4),
                listOf(3, 8, 11),
                listOf(3, 9, 10),
                listOf(3, 10, 9),
                listOf(3, 11, 8),
                listOf(3, 12, 15),
                listOf(3, 13, 14),
                listOf(3, 14, 13),
                listOf(3, 15, 12),
                listOf(4, 0, 4),
                listOf(4, 1, 5),
                listOf(4, 2, 6),
                listOf(4, 3, 7),
                listOf(4, 4, 0),
                listOf(4, 5, 1),
                listOf(4, 6, 2),
                listOf(4, 7, 3),
                listOf(4, 8, 12),
                listOf(4, 9, 13),
                listOf(4, 10, 14),
                listOf(4, 11, 15),
                listOf(4, 12, 8),
                listOf(4, 13, 9),
                listOf(4, 14, 10),
                listOf(4, 15, 11),
                listOf(5, 0, 5),
                listOf(5, 1, 4),
                listOf(5, 2, 7),
                listOf(5, 3, 6),
                listOf(5, 4, 1),
                listOf(5, 5, 0),
                listOf(5, 6, 3),
                listOf(5, 7, 2),
                listOf(5, 8, 13),
                listOf(5, 9, 12),
                listOf(5, 10, 15),
                listOf(5, 11, 14),
                listOf(5, 12, 9),
                listOf(5, 13, 8),
                listOf(5, 14, 11),
                listOf(5, 15, 10),
                listOf(6, 0, 6),
                listOf(6, 1, 7),
                listOf(6, 2, 4),
                listOf(6, 3, 5),
                listOf(6, 4, 2),
                listOf(6, 5, 3),
                listOf(6, 6, 0),
                listOf(6, 7, 1),
                listOf(6, 8, 14),
                listOf(6, 9, 15),
                listOf(6, 10, 12),
                listOf(6, 11, 13),
                listOf(6, 12, 10),
                listOf(6, 13, 11),
                listOf(6, 14, 8),
                listOf(6, 15, 9),
                listOf(7, 0, 7),
                listOf(7, 1, 6),
                listOf(7, 2, 5),
                listOf(7, 3, 4),
                listOf(7, 4, 3),
                listOf(7, 5, 2),
                listOf(7, 6, 1),
                listOf(7, 7, 0),
                listOf(7, 8, 15),
                listOf(7, 9, 14),
                listOf(7, 10, 13),
                listOf(7, 11, 12),
                listOf(7, 12, 11),
                listOf(7, 13, 10),
                listOf(7, 14, 9),
                listOf(7, 15, 8),
                listOf(8, 0, 8),
                listOf(8, 1, 9),
                listOf(8, 2, 10),
                listOf(8, 3, 11),
                listOf(8, 4, 12),
                listOf(8, 5, 13),
                listOf(8, 6, 14),
                listOf(8, 7, 15),
                listOf(8, 8, 0),
                listOf(8, 9, 1),
                listOf(8, 10, 2),
                listOf(8, 11, 3),
                listOf(8, 12, 4),
                listOf(8, 13, 5),
                listOf(8, 14, 6),
                listOf(8, 15, 7),
                listOf(9, 0, 9),
                listOf(9, 1, 8),
                listOf(9, 2, 11),
                listOf(9, 3, 10),
                listOf(9, 4, 13),
                listOf(9, 5, 12),
                listOf(9, 6, 15),
                listOf(9, 7, 14),
                listOf(9, 8, 1),
                listOf(9, 9, 0),
                listOf(9, 10, 3),
                listOf(9, 11, 2),
                listOf(9, 12, 5),
                listOf(9, 13, 4),
                listOf(9, 14, 7),
                listOf(9, 15, 6),
                listOf(10, 0, 10),
                listOf(10, 1, 11),
                listOf(10, 2, 8),
                listOf(10, 3, 9),
                listOf(10, 4, 14),
                listOf(10, 5, 15),
                listOf(10, 6, 12),
                listOf(10, 7, 13),
                listOf(10, 8, 2),
                listOf(10, 9, 3),
                listOf(10, 10, 0),
                listOf(10, 11, 1),
                listOf(10, 12, 6),
                listOf(10, 13, 7),
                listOf(10, 14, 4),
                listOf(10, 15, 5),
                listOf(11, 0, 11),
                listOf(11, 1, 10),
                listOf(11, 2, 9),
                listOf(11, 3, 8),
                listOf(11, 4, 15),
                listOf(11, 5, 14),
                listOf(11, 6, 13),
                listOf(11, 7, 12),
                listOf(11, 8, 3),
                listOf(11, 9, 2),
                listOf(11, 10, 1),
                listOf(11, 11, 0),
                listOf(11, 12, 7),
                listOf(11, 13, 6),
                listOf(11, 14, 5),
                listOf(11, 15, 4),
                listOf(12, 0, 12),
                listOf(12, 1, 13),
                listOf(12, 2, 14),
                listOf(12, 3, 15),
                listOf(12, 4, 8),
                listOf(12, 5, 9),
                listOf(12, 6, 10),
                listOf(12, 7, 11),
                listOf(12, 8, 4),
                listOf(12, 9, 5),
                listOf(12, 10, 6),
                listOf(12, 11, 7),
                listOf(12, 12, 0),
                listOf(12, 13, 1),
                listOf(12, 14, 2),
                listOf(12, 15, 3),
                listOf(13, 0, 13),
                listOf(13, 1, 12),
                listOf(13, 2, 15),
                listOf(13, 3, 14),
                listOf(13, 4, 9),
                listOf(13, 5, 8),
                listOf(13, 6, 11),
                listOf(13, 7, 10),
                listOf(13, 8, 5),
                listOf(13, 9, 4),
                listOf(13, 10, 7),
                listOf(13, 11, 6),
                listOf(13, 12, 1),
                listOf(13, 13, 0),
                listOf(13, 14, 3),
                listOf(13, 15, 2),
                listOf(14, 0, 14),
                listOf(14, 1, 15),
                listOf(14, 2, 12),
                listOf(14, 3, 13),
                listOf(14, 4, 10),
                listOf(14, 5, 11),
                listOf(14, 6, 8),
                listOf(14, 7, 9),
                listOf(14, 8, 6),
                listOf(14, 9, 7),
                listOf(14, 10, 4),
                listOf(14, 11, 5),
                listOf(14, 12, 2),
                listOf(14, 13, 3),
                listOf(14, 14, 0),
                listOf(14, 15, 1),
                listOf(15, 0, 15),
                listOf(15, 1, 14),
                listOf(15, 2, 13),
                listOf(15, 3, 12),
                listOf(15, 4, 11),
                listOf(15, 5, 10),
                listOf(15, 6, 9),
                listOf(15, 7, 8),
                listOf(15, 8, 7),
                listOf(15, 9, 6),
                listOf(15, 10, 5),
                listOf(15, 11, 4),
                listOf(15, 12, 3),
                listOf(15, 13, 2),
                listOf(15, 14, 1),
                listOf(15, 15, 0),
            )
        )
        assertEquals(readTextFrom("function/table"), mapper.writeValueAsString(nibbleXOR))
    }

    @Test
    fun deserializeTable() {
        val nibbleXOR = Table(
            listOf(
                listOf(0, 0, 0),
                listOf(0, 1, 1),
                listOf(0, 2, 2),
                listOf(0, 3, 3),
                listOf(0, 4, 4),
                listOf(0, 5, 5),
                listOf(0, 6, 6),
                listOf(0, 7, 7),
                listOf(0, 8, 8),
                listOf(0, 9, 9),
                listOf(0, 10, 10),
                listOf(0, 11, 11),
                listOf(0, 12, 12),
                listOf(0, 13, 13),
                listOf(0, 14, 14),
                listOf(0, 15, 15),
                listOf(1, 0, 1),
                listOf(1, 1, 0),
                listOf(1, 2, 3),
                listOf(1, 3, 2),
                listOf(1, 4, 5),
                listOf(1, 5, 4),
                listOf(1, 6, 7),
                listOf(1, 7, 6),
                listOf(1, 8, 9),
                listOf(1, 9, 8),
                listOf(1, 10, 11),
                listOf(1, 11, 10),
                listOf(1, 12, 13),
                listOf(1, 13, 12),
                listOf(1, 14, 15),
                listOf(1, 15, 14),
                listOf(2, 0, 2),
                listOf(2, 1, 3),
                listOf(2, 2, 0),
                listOf(2, 3, 1),
                listOf(2, 4, 6),
                listOf(2, 5, 7),
                listOf(2, 6, 4),
                listOf(2, 7, 5),
                listOf(2, 8, 10),
                listOf(2, 9, 11),
                listOf(2, 10, 8),
                listOf(2, 11, 9),
                listOf(2, 12, 14),
                listOf(2, 13, 15),
                listOf(2, 14, 12),
                listOf(2, 15, 13),
                listOf(3, 0, 3),
                listOf(3, 1, 2),
                listOf(3, 2, 1),
                listOf(3, 3, 0),
                listOf(3, 4, 7),
                listOf(3, 5, 6),
                listOf(3, 6, 5),
                listOf(3, 7, 4),
                listOf(3, 8, 11),
                listOf(3, 9, 10),
                listOf(3, 10, 9),
                listOf(3, 11, 8),
                listOf(3, 12, 15),
                listOf(3, 13, 14),
                listOf(3, 14, 13),
                listOf(3, 15, 12),
                listOf(4, 0, 4),
                listOf(4, 1, 5),
                listOf(4, 2, 6),
                listOf(4, 3, 7),
                listOf(4, 4, 0),
                listOf(4, 5, 1),
                listOf(4, 6, 2),
                listOf(4, 7, 3),
                listOf(4, 8, 12),
                listOf(4, 9, 13),
                listOf(4, 10, 14),
                listOf(4, 11, 15),
                listOf(4, 12, 8),
                listOf(4, 13, 9),
                listOf(4, 14, 10),
                listOf(4, 15, 11),
                listOf(5, 0, 5),
                listOf(5, 1, 4),
                listOf(5, 2, 7),
                listOf(5, 3, 6),
                listOf(5, 4, 1),
                listOf(5, 5, 0),
                listOf(5, 6, 3),
                listOf(5, 7, 2),
                listOf(5, 8, 13),
                listOf(5, 9, 12),
                listOf(5, 10, 15),
                listOf(5, 11, 14),
                listOf(5, 12, 9),
                listOf(5, 13, 8),
                listOf(5, 14, 11),
                listOf(5, 15, 10),
                listOf(6, 0, 6),
                listOf(6, 1, 7),
                listOf(6, 2, 4),
                listOf(6, 3, 5),
                listOf(6, 4, 2),
                listOf(6, 5, 3),
                listOf(6, 6, 0),
                listOf(6, 7, 1),
                listOf(6, 8, 14),
                listOf(6, 9, 15),
                listOf(6, 10, 12),
                listOf(6, 11, 13),
                listOf(6, 12, 10),
                listOf(6, 13, 11),
                listOf(6, 14, 8),
                listOf(6, 15, 9),
                listOf(7, 0, 7),
                listOf(7, 1, 6),
                listOf(7, 2, 5),
                listOf(7, 3, 4),
                listOf(7, 4, 3),
                listOf(7, 5, 2),
                listOf(7, 6, 1),
                listOf(7, 7, 0),
                listOf(7, 8, 15),
                listOf(7, 9, 14),
                listOf(7, 10, 13),
                listOf(7, 11, 12),
                listOf(7, 12, 11),
                listOf(7, 13, 10),
                listOf(7, 14, 9),
                listOf(7, 15, 8),
                listOf(8, 0, 8),
                listOf(8, 1, 9),
                listOf(8, 2, 10),
                listOf(8, 3, 11),
                listOf(8, 4, 12),
                listOf(8, 5, 13),
                listOf(8, 6, 14),
                listOf(8, 7, 15),
                listOf(8, 8, 0),
                listOf(8, 9, 1),
                listOf(8, 10, 2),
                listOf(8, 11, 3),
                listOf(8, 12, 4),
                listOf(8, 13, 5),
                listOf(8, 14, 6),
                listOf(8, 15, 7),
                listOf(9, 0, 9),
                listOf(9, 1, 8),
                listOf(9, 2, 11),
                listOf(9, 3, 10),
                listOf(9, 4, 13),
                listOf(9, 5, 12),
                listOf(9, 6, 15),
                listOf(9, 7, 14),
                listOf(9, 8, 1),
                listOf(9, 9, 0),
                listOf(9, 10, 3),
                listOf(9, 11, 2),
                listOf(9, 12, 5),
                listOf(9, 13, 4),
                listOf(9, 14, 7),
                listOf(9, 15, 6),
                listOf(10, 0, 10),
                listOf(10, 1, 11),
                listOf(10, 2, 8),
                listOf(10, 3, 9),
                listOf(10, 4, 14),
                listOf(10, 5, 15),
                listOf(10, 6, 12),
                listOf(10, 7, 13),
                listOf(10, 8, 2),
                listOf(10, 9, 3),
                listOf(10, 10, 0),
                listOf(10, 11, 1),
                listOf(10, 12, 6),
                listOf(10, 13, 7),
                listOf(10, 14, 4),
                listOf(10, 15, 5),
                listOf(11, 0, 11),
                listOf(11, 1, 10),
                listOf(11, 2, 9),
                listOf(11, 3, 8),
                listOf(11, 4, 15),
                listOf(11, 5, 14),
                listOf(11, 6, 13),
                listOf(11, 7, 12),
                listOf(11, 8, 3),
                listOf(11, 9, 2),
                listOf(11, 10, 1),
                listOf(11, 11, 0),
                listOf(11, 12, 7),
                listOf(11, 13, 6),
                listOf(11, 14, 5),
                listOf(11, 15, 4),
                listOf(12, 0, 12),
                listOf(12, 1, 13),
                listOf(12, 2, 14),
                listOf(12, 3, 15),
                listOf(12, 4, 8),
                listOf(12, 5, 9),
                listOf(12, 6, 10),
                listOf(12, 7, 11),
                listOf(12, 8, 4),
                listOf(12, 9, 5),
                listOf(12, 10, 6),
                listOf(12, 11, 7),
                listOf(12, 12, 0),
                listOf(12, 13, 1),
                listOf(12, 14, 2),
                listOf(12, 15, 3),
                listOf(13, 0, 13),
                listOf(13, 1, 12),
                listOf(13, 2, 15),
                listOf(13, 3, 14),
                listOf(13, 4, 9),
                listOf(13, 5, 8),
                listOf(13, 6, 11),
                listOf(13, 7, 10),
                listOf(13, 8, 5),
                listOf(13, 9, 4),
                listOf(13, 10, 7),
                listOf(13, 11, 6),
                listOf(13, 12, 1),
                listOf(13, 13, 0),
                listOf(13, 14, 3),
                listOf(13, 15, 2),
                listOf(14, 0, 14),
                listOf(14, 1, 15),
                listOf(14, 2, 12),
                listOf(14, 3, 13),
                listOf(14, 4, 10),
                listOf(14, 5, 11),
                listOf(14, 6, 8),
                listOf(14, 7, 9),
                listOf(14, 8, 6),
                listOf(14, 9, 7),
                listOf(14, 10, 4),
                listOf(14, 11, 5),
                listOf(14, 12, 2),
                listOf(14, 13, 3),
                listOf(14, 14, 0),
                listOf(14, 15, 1),
                listOf(15, 0, 15),
                listOf(15, 1, 14),
                listOf(15, 2, 13),
                listOf(15, 3, 12),
                listOf(15, 4, 11),
                listOf(15, 5, 10),
                listOf(15, 6, 9),
                listOf(15, 7, 8),
                listOf(15, 8, 7),
                listOf(15, 9, 6),
                listOf(15, 10, 5),
                listOf(15, 11, 4),
                listOf(15, 12, 3),
                listOf(15, 13, 2),
                listOf(15, 14, 1),
                listOf(15, 15, 0),
            )
        )
        assertEquals(nibbleXOR, readJsonFrom<Table>("function/table"))
    }

    @Test
    fun serializeCommand() {
        val userCommand = Command("user_command")
        assertEquals(readTextFrom("function/command"), mapper.writeValueAsString(userCommand))
    }

    @Test
    fun deserializeCommand() {
        val userCommand = Command("user_command")
        assertEquals(userCommand, readJsonFrom<Command>("function/command"))
    }

    @Test
    fun serializeGFMul() {
        val aesPoly = GFMul(0x11B, 0x53)
        assertEquals(readTextFrom("function/gfmul"), mapper.writeValueAsString(aesPoly))
    }

    @Test
    fun deserializeGFMul() {
        val aesPoly = GFMul(0x11B, 0x53)
        assertEquals(aesPoly, readJsonFrom<GFMul>("function/gfmul"))
    }

    @Test
    fun serializeGFMatToVecMul() {
        val aesMatMul = GFMatToVecMul(
            0x11B,
            listOf(
                listOf(2, 3, 1, 1),
                listOf(1, 2, 3, 1),
                listOf(1, 1, 2, 3),
                listOf(3, 1, 1, 2)
            )
        )
        assertEquals(readTextFrom("function/gfmattovecmul"), mapper.writeValueAsString(aesMatMul))
    }


    @Test
    fun deserializeGFMatToVecMul() {
        val aesMatMul = GFMatToVecMul(
            0x11B,
            listOf(
                listOf(2, 3, 1, 1),
                listOf(1, 2, 3, 1),
                listOf(1, 1, 2, 3),
                listOf(3, 1, 1, 2)
            )
        )
        assertEquals(aesMatMul, readJsonFrom<GFMatToVecMul>("function/gfmattovecmul"))
    }

    @Test
    fun serializeLFSR() {
        val lfsr = LFSR(8, 0b0101, Direction.ShiftLeft)
        assertEquals(readTextFrom("function/lfsr"), mapper.writeValueAsString(lfsr))
    }

    @Test
    fun deserializeLFSR() {
        val lfsr = LFSR(8, 0b0101, Direction.ShiftLeft)
        assertEquals(lfsr, readJsonFrom<LFSR>("function/lfsr"))
    }

    @Test
    fun serializeSplit() {
        val split = Split(
            listOf(
                listOf(7, 6, 5, 4),
                listOf(3, 2, 1, 0),
            )
        )
        assertEquals(readTextFrom("function/split"), mapper.writeValueAsString(split))
    }

    @Test
    fun deserializeSplit() {
        val split = Split(
            listOf(
                listOf(7, 6, 5, 4),
                listOf(3, 2, 1, 0),
            )
        )
        assertEquals(split, readJsonFrom<Split>("function/split"))
    }

    @Test
    fun serializeConcat() {
        val concat = Concat(
            listOf(
                BitAndWord(1, 1),
                BitAndWord(0, 1),
                BitAndWord(1, 0),
                BitAndWord(0, 0)
            )
        )
        assertEquals(readTextFrom("function/concat"), mapper.writeValueAsString(concat))
    }

    @Test
    fun deserializeConcat() {
        val concat = Concat(
            listOf(
                BitAndWord(1, 1),
                BitAndWord(0, 1),
                BitAndWord(1, 0),
                BitAndWord(0, 0)
            )
        )
        assertEquals(concat, readJsonFrom<Concat>("function/concat"))
    }

    @Test
    fun serializeShift() {
        val shift = Shift(
            8, 3, Direction.ShiftRight
        )
        assertEquals(readTextFrom("function/shift"), mapper.writeValueAsString(shift))
    }

    @Test
    fun deserializeShift() {
        val shift = Shift(
            8, 3, Direction.ShiftRight
        )
        assertEquals(shift, readJsonFrom<Shift>("function/shift"))
    }

    @Test
    fun serializeCircularShift() {
      val circularShift = CircularShift(
          8, 6, Direction.ShiftLeft
      )
      assertEquals(readTextFrom("function/circular_shift"), mapper.writeValueAsString(circularShift))
    }

    @Test
    fun deserializeCircularShift() {
      val circularShift = CircularShift(
          8, 6, Direction.ShiftLeft
      )
      assertEquals(circularShift, readJsonFrom<CircularShift>("function/circular_shift"))
    }

    @Test
    fun serializeAndConst() {
      val andConst = AndConst(
          83
      )
      assertEquals(readTextFrom("function/and_const"), mapper.writeValueAsString(andConst))
    }

    @Test
    fun deserializeAndConst() {
      val andConst = AndConst(
          83
      )
      assertEquals(andConst, readJsonFrom<AndConst>("function/and_const"))
    }

    @Test
    fun serializeOrConst() {
      val orConst = OrConst(
          83
      )
      assertEquals(readTextFrom("function/or_const"), mapper.writeValueAsString(orConst))
    }

    @Test
    fun deserializeOrConst() {
      val orConst = OrConst(
          83
      )
      assertEquals(orConst, readJsonFrom<OrConst>("function/or_const"))
    }

    @Test
    fun serializeBitAndWord() {
        val bitAndWord = BitAndWord(1, 2)
        assertEquals(readTextFrom("bit_and_word"), mapper.writeValueAsString(bitAndWord))
    }

    @Test
    fun deserializeBitAndWord() {
        val bitAndWord = BitAndWord(1, 2)
        assertEquals(bitAndWord, readJsonFrom<BitAndWord>("bit_and_word"))
    }

    @Test
    fun serializeShiftLeft() {
        assertEquals(readTextFrom("shiftleft"), mapper.writeValueAsString(Direction.ShiftLeft))
    }

    @Test
    fun deserializeShiftLeft() {
        assertEquals(Direction.ShiftLeft, readJsonFrom<Direction>("shiftleft"))
    }

    @Test
    fun serializeShiftRight() {
        assertEquals(readTextFrom("shiftright"), mapper.writeValueAsString(Direction.ShiftRight))
    }

    @Test
    fun deserializeShiftRight() {
        assertEquals(Direction.ShiftRight, readJsonFrom<Direction>("shiftright"))
    }

    @Test
    fun serializeTransition() {
        val transition = Transition(
            listOf(StateNodeUID(0)),
            FunctionNodeUID(0),
            listOf(StateNodeUID(1))
        )
        assertEquals(readTextFrom("transition"), mapper.writeValueAsString(transition))
    }

    @Test
    fun deserializeTransition() {
        val transition = Transition(
            listOf(StateNodeUID(0)),
            FunctionNodeUID(0),
            listOf(StateNodeUID(1))
        )
        assertEquals(transition, readJsonFrom<Transition>("transition"))
    }

    @Test
    fun serializeDifferentialEqual() {
        val differentialEqual = Linear(Equal)
        assertEquals(readTextFrom("differential_equal"), mapper.writeValueAsString(differentialEqual))
    }

    @Test
    fun deserializeDifferentialEqual() {
        val differentialEqual = Linear(Equal)
        assertEquals(differentialEqual, readJsonFrom<Linear>("differential_equal"))
    }
}