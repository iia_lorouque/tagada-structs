package fr.limos.gitlab.decrypt.tagada.structs

import fr.limos.gitlab.decrypt.tagada.structs.common.Range
import fr.limos.gitlab.decrypt.tagada.structs.differential.*
import fr.limos.gitlab.decrypt.tagada.structs.specification.Equal
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class Differential {

    @Test
    fun serializeDDT() {
        val skinnyDDT = DDT(
            mapOf(
                DDTArgs(0, 0) to MinusLog2Probability(Hundredth(0)),
                DDTArgs(1, 8) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(1, 9) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(1, 10) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(1, 11) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(2, 1) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(2, 3) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(2, 5) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(2, 6) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(3, 8) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(3, 9) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(3, 10) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(3, 11) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(3, 12) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(3, 13) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(3, 14) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(3, 15) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(4, 2) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(4, 6) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(4, 7) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(4, 11) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(4, 12) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(4, 13) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(5, 2) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(5, 6) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(5, 7) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(5, 10) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(5, 12) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(5, 13) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(6, 1) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(6, 3) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(6, 4) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(6, 7) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(6, 8) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(6, 10) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(6, 13) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(6, 14) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(7, 1) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(7, 3) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(7, 4) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(7, 7) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(7, 9) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(7, 11) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(7, 12) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(7, 15) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(8, 4) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(8, 5) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(8, 12) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(8, 13) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(8, 14) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(8, 15) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(9, 4) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(9, 5) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(9, 12) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(9, 13) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(9, 14) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(9, 15) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(10, 5) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(10, 6) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(10, 8) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(10, 9) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(10, 10) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(10, 11) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(11, 1) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(11, 3) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(11, 12) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(11, 13) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(11, 14) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(11, 15) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(12, 2) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(12, 6) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(12, 7) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(12, 8) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(12, 14) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(12, 15) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(13, 2) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(13, 6) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(13, 7) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(13, 9) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(13, 14) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(13, 15) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(14, 1) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(14, 3) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(14, 4) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(14, 7) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(14, 9) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(14, 11) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(14, 13) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(14, 14) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(15, 1) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(15, 3) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(15, 4) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(15, 7) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(15, 8) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(15, 10) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(15, 12) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(15, 15) to MinusLog2Probability(Hundredth(300))
            )
        )
        assertEquals(readTextFrom("differential/ddt"), mapper.writeValueAsString(skinnyDDT))
    }

    @Test
    fun deserializeDDT() {
        val skinnyDDT = DDT(
            mapOf(
                DDTArgs(0, 0) to MinusLog2Probability(Hundredth(0)),
                DDTArgs(1, 8) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(1, 9) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(1, 10) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(1, 11) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(2, 1) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(2, 3) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(2, 5) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(2, 6) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(3, 8) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(3, 9) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(3, 10) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(3, 11) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(3, 12) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(3, 13) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(3, 14) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(3, 15) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(4, 2) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(4, 6) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(4, 7) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(4, 11) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(4, 12) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(4, 13) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(5, 2) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(5, 6) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(5, 7) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(5, 10) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(5, 12) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(5, 13) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(6, 1) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(6, 3) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(6, 4) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(6, 7) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(6, 8) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(6, 10) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(6, 13) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(6, 14) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(7, 1) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(7, 3) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(7, 4) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(7, 7) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(7, 9) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(7, 11) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(7, 12) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(7, 15) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(8, 4) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(8, 5) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(8, 12) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(8, 13) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(8, 14) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(8, 15) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(9, 4) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(9, 5) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(9, 12) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(9, 13) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(9, 14) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(9, 15) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(10, 5) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(10, 6) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(10, 8) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(10, 9) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(10, 10) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(10, 11) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(11, 1) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(11, 3) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(11, 12) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(11, 13) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(11, 14) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(11, 15) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(12, 2) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(12, 6) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(12, 7) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(12, 8) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(12, 14) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(12, 15) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(13, 2) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(13, 6) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(13, 7) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(13, 9) to MinusLog2Probability(Hundredth(200)),
                DDTArgs(13, 14) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(13, 15) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(14, 1) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(14, 3) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(14, 4) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(14, 7) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(14, 9) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(14, 11) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(14, 13) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(14, 14) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(15, 1) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(15, 3) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(15, 4) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(15, 7) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(15, 8) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(15, 10) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(15, 12) to MinusLog2Probability(Hundredth(300)),
                DDTArgs(15, 15) to MinusLog2Probability(Hundredth(300))
            )
        )
        assertEquals(skinnyDDT, readJsonFrom("differential/ddt"))
    }

    @Test
    fun serializeLinear() {
        val equal = Linear(Equal)
        assertEquals(readTextFrom("differential/linear"), mapper.writeValueAsString(equal))
    }

    @Test
    fun deserializeLinear() {
        val equal = Linear(Equal)
        assertEquals(equal, readJsonFrom("differential/linear"))
    }

    @Test
    fun serializeDifferentialFunctionNode() {
        val byte = Range(0, 256)
        val functionNode = DifferentialFunctionNode(
            listOf(byte),
            listOf(byte),
            Linear(Equal)
        )
        assertEquals(readTextFrom("differential/function_node"), mapper.writeValueAsString(functionNode))
    }


    @Test
    fun deserializeDifferentialFunctionNode() {
        val byte = Range(0, 256)
        val functionNode = DifferentialFunctionNode(
            listOf(byte),
            listOf(byte),
            Linear(Equal)
        )
        assertEquals(functionNode, readJsonFrom("differential/function_node"))
    }

    @Test
    fun serializeDifferentialFunctionNodeUID() {
        val fUID = DifferentialFunctionUID(10)
        assertEquals(readTextFrom("function_node_uid"), mapper.writeValueAsString(fUID))
    }

    @Test
    fun deserializeDifferentialFunctionNodeUID() {
        val fUID = DifferentialFunctionUID(10)
        assertEquals(fUID, readJsonFrom("function_node_uid"))
    }

    @Test
    fun serializeDifferentialVariable() {
        val variable = Variable(Range(0, 256))
        assertEquals(readTextFrom("differential/variable"), mapper.writeValueAsString(variable))
    }

    @Test
    fun deserializeDifferentialVariable() {
        val variable = Variable(Range(0, 256))
        assertEquals(variable, readJsonFrom("differential/variable"))
    }


    @Test
    fun serializeDifferentialProbability() {
        val probability = Probability(setOf(
            MinusLog2Probability(Hundredth(700)),
            MinusLog2Probability(Hundredth(600)),
            MinusLog2Probability(Hundredth(0)),
        ))
        assertEquals(readTextFrom("differential/probability"), mapper.writeValueAsString(probability))
    }
    @Test
    fun deserializeDifferentialProbability() {
        val probability = Probability(setOf(
            MinusLog2Probability(Hundredth(700)),
            MinusLog2Probability(Hundredth(600)),
            MinusLog2Probability(Hundredth(0)),
        ))
        assertEquals(probability, readJsonFrom("differential/probability"))
    }

    @Test
    fun serializeConstant() {
        val constant = Constant(13)
        assertEquals(readTextFrom("differential/constant"), mapper.writeValueAsString(constant))
    }
    @Test
    fun deserializeConstant() {
        val constant = Constant(13)
        assertEquals(constant, readJsonFrom("differential/constant"))
    }

}