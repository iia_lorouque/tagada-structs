use std::collections::HashMap;
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use crate::common::{Domain,Version,Value};
use crate::differential::MinusLog2Probability;

/// Represents a cipher Dag (directed acyclic graph) in Tagada.
/// - `version`: the library version that generates the JSON
/// - `states`: the states of the cipher. The states include the plaintext, the key, the ciphertext, the internal
/// - `states`: and the constants of the cipher
/// - `functions`: the functions used in the cipher
/// - `transitions`: the links between the states using one of the cipher function
#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
pub struct TruncatedDifferentialGraph {
    pub version: Version,
    pub states: Vec<TruncatedDifferentialStateNode>,
    pub functions: Vec<TruncatedDifferentialFunctionNode>,
    pub transitions: Vec<TruncatedDifferentialTransition>,
    pub plaintext: Vec<TruncatedDifferentialStateNodeUID>,
    pub key: Vec<TruncatedDifferentialStateNodeUID>,
    pub ciphertext: Vec<TruncatedDifferentialStateNodeUID>,
    pub names: HashMap<String, TruncatedDifferentialStateNodeUID>,
    pub blocks: HashMap<String, Vec<TruncatedDifferentialStateNodeUID>>,
}

/// Represents a state of the cipher
/// - `domain`: the accepted domain of the state
/// - `value`: the value of the state. When the state is a constant, the value must be set to a value that belongs to
/// the domain. If the state is a constant, the value must be set to None.
#[derive(Serialize, Deserialize, Debug, Clone, Hash, Eq, PartialEq)]
pub enum TruncatedDifferentialStateNode {
    Variable { domain: Domain },
    Constant { value: Value }
}

/// Defines a function node. A function is defined by a domain, a co-domain and a mapping.
/// - `domain`: the domain of the function is represented by a cartesian product of the domain of the input parameters
/// - `co_domain`: the domain of the function is represented by a cartesian product of the domain of the output parameters
/// - `function`: the mapping function
#[derive(Serialize, Deserialize, Debug, Clone, Hash, Eq, PartialEq)]
pub struct TruncatedDifferentialFunctionNode {
    pub domain: Vec<Domain>,
    pub co_domain: Vec<Domain>,
    pub function: TruncatedDifferentialFunction,
}

/// Represents the managed functions.
#[derive(Serialize, Deserialize, Debug, Clone, Hash, Eq, PartialEq)]
#[serde(tag = "type")]
pub enum TruncatedDifferentialFunction {
    NonLinearTransition { maximum_activation_probability: MinusLog2Probability },
    Linear { truth_table: Vec<Vec<bool>> }
}

/// Represents a function call in the cipher in the form of function(inputs) = outputs
#[derive(Serialize, Deserialize, Debug, Clone, Hash, Eq, PartialEq)]
pub struct TruncatedDifferentialTransition {
    pub inputs: Vec<TruncatedDifferentialStateNodeUID>,
    pub function: TruncatedDifferentialFunctionNodeUID,
    pub outputs: Vec<TruncatedDifferentialStateNodeUID>,
}

/// A function node UID
#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq)]
pub struct TruncatedDifferentialFunctionNodeUID {
    pub uid: usize,
}

impl Serialize for TruncatedDifferentialFunctionNodeUID {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error> where S: Serializer {
        self.uid.serialize(serializer)
    }
}

impl<'de> Deserialize<'de> for TruncatedDifferentialFunctionNodeUID {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error> where D: Deserializer<'de> {
        Deserialize::deserialize(deserializer)
            .map(|uid: usize| TruncatedDifferentialFunctionNodeUID { uid })
    }
}

#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq)]
pub struct TruncatedDifferentialStateNodeUID {
    pub uid: usize,
}

impl Serialize for TruncatedDifferentialStateNodeUID {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error> where S: Serializer {
        self.uid.serialize(serializer)
    }
}

impl<'de> Deserialize<'de> for TruncatedDifferentialStateNodeUID {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error> where D: Deserializer<'de> {
        Deserialize::deserialize(deserializer)
            .map(|uid: usize| TruncatedDifferentialStateNodeUID { uid })
    }
}

