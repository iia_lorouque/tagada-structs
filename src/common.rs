use std::collections::BTreeSet;
use serde::{Serialize, Deserialize};

pub type Value = usize;

/// Represents an API version in the format MAJOR.MINOR.PATCH
#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
pub struct Version(pub i32, pub i32, pub i32);

/// Represents the managed Domains
#[derive(Serialize, Deserialize, Debug, Clone, Hash, Eq, PartialEq)]
#[serde(tag = "type")]
pub enum Domain {
    /// Represents a half open range [min; max[
    Range { min: Value, max: Value },
    /// Represents a set of values
    Sparse { values: BTreeSet<Value> },
}

#[cfg(test)]
pub mod tests {
    use std::fs::File;
    use std::io;
    use std::io::{BufReader, ErrorKind, Read};
    use serde::de;
    use serde_json::ser::to_string;
    use crate::common::{Version,Domain};

    pub fn read_text_from(path: &str) -> io::Result<String> {
        let file = File::open(format!("src/test/resources/{}.json", path))?;
        let mut read = BufReader::new(file);
        let mut json = String::new();
        read.read_to_string(&mut json)?;
        Ok(json)
    }

    pub fn read_json_from<T: de::DeserializeOwned>(path: &str) -> io::Result<T> {
        let file = File::open(format!("src/test/resources/{}.json", path))?;
        match serde_json::de::from_reader(file) {
            Ok(inner) => Ok(inner),
            Err(err) => Err(io::Error::new(ErrorKind::InvalidData, err))
        }
    }

    #[test]
    fn serialize_version() -> io::Result<()> {
        let version = Version(1, 2, 3);
        assert_eq!(read_text_from("version")?, to_string(&version)?);
        Ok(())
    }

    #[test]
    fn deserialize_version() -> io::Result<()> {
        let version = Version(1, 2, 3);
        assert_eq!(version, read_json_from("version")?);
        Ok(())
    }

    #[test]
    fn serialize_range_domain() -> io::Result<()> {
        let nibble = Domain::Range { min: 0, max: 16 };
        assert_eq!(read_text_from("domain/range")?, to_string(&nibble)?);
        Ok(())
    }

    #[test]
    fn deserialize_range_domain() -> io::Result<()> {
        let nibble = Domain::Range { min: 0, max: 16 };
        assert_eq!(nibble, read_json_from("domain/range")?);
        Ok(())
    }

    #[test]
    fn serialize_sparse_domain() -> io::Result<()> {
        let nibble = Domain::Sparse { values: (0..16).collect() };
        assert_eq!(read_text_from("domain/sparse")?, to_string(&nibble)?);
        Ok(())
    }

    #[test]
    fn deserialize_sparse_domain() -> io::Result<()> {
        let nibble = Domain::Sparse { values: (0..16).collect() };
        assert_eq!(nibble, read_json_from("domain/sparse")?);
        Ok(())
    }
}