extern crate core;

use crate::differential::DifferentialGraph;
use crate::specification::SpecificationGraph;
use crate::truncated_differential::TruncatedDifferentialGraph;
use serde::{Serialize, Deserialize};

mod computation;
pub mod differential;
pub mod truncated_differential;
pub mod specification;
pub mod common;

#[derive(Serialize, Deserialize, Eq, PartialEq, Clone, Debug)]
#[serde(tag = "type")]
pub enum TagadaGraph {
    Specification { graph: SpecificationGraph },
    Differential { graph: DifferentialGraph },
    TruncatedDifferential { graph: TruncatedDifferentialGraph },
}