# Tagada Structs

Latest version: `1.0.0`

This repository contains utilities to read/write Tagada cipher DAGs. It is developped for two languages which are 
[Rust](https://www.rust-lang.org/) and [Kotlin](https://kotlinlang.org/).

## Usage

### Rust

To use the library add the following line in the `Cargo.toml` file:

```toml
tagada-structs = { git = "https://gitlab.limos.fr/iia_lorouque/tagada-structs", tag = "1.1.0" }
```

#### Example

```toml
# Cargo.toml
[package]
name = "tagada-structs-bin"
version = "0.1.0"
edition = "2021"

# See more keys and their definitions at https://doc.rust-lang.org/cargo/reference/manifest.html

[dependencies]
tagada-structs = { git = "https://gitlab.limos.fr/iia_lorouque/tagada-structs", tag = "1.1.0" }
```

### Kotlin

We recommend to use gradle with the Kotlin DSL. To use the library add the following line:

```kotlin
implementation("fr.limos.gitlab.decrypt.tagada:tagada-structs:1.1.0")
```

in the `dependencies {}` block of the `build.gradle.kts` file, the line:

```kotlin
import java.net.URI
```

at the beginning of `settings.gradle.kts` and the block:

```kotlin
sourceControl {
    gitRepository(URI("https://gitlab.limos.fr/iia_lorouque/tagada-structs")) {
        producesModule("fr.limos.gitlab.decrypt.tagada:tagada-structs")
    }
}
```

at the end of `settings.gradle.kts`.

#### Example

```kotlin
// build.gradle.kts
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.7.20"
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation("fr.limos.gitlab.decrypt.tagada:tagada-structs:1.1.0")
    testImplementation(kotlin("test"))
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}
```

```kotlin
// settings.gradle.kts
import java.net.URI

rootProject.name = "tagada-structs-bin"

sourceControl {
    gitRepository(URI("https://gitlab.limos.fr/iia_lorouque/tagada-structs")) {
        producesModule("fr.limos.gitlab.decrypt.tagada:tagada-structs")
    }
}
```

## Changelog

`1.1.0`

Add inplace computation for the rust library.

`1.0.0`

Initial stable release.

### Operators list

|         Operator         |                  Semantic                   | Linearity  | Support |
|:------------------------:|:-------------------------------------------:|:----------:|:--------|
|           $=$            |                  Equality                   | yes        | yes     |
|         $\oplus$         |   Bitwise XOR (or Galois Field addition)    | yes        | yes     |
|   $\otimes_\mathbb{N}$   | Galois Field multiplication with a constant | yes        | yes     |
|          $LFSR$          |       Linear feedback shift register        | yes        | yes     |
|     $\ll_\mathbb{N}$     |             Left shift register             | yes        | yes     |
|     $\gg_\mathbb{N}$     |             Left shift register             | yes        | yes     |
|          $\pi$           |              Bits permutation               | yes        | yes     |
|         $\|\| $          |          Concatenation (or split)           | yes        | yes     |
| $\mathtt{\&}_\mathbb{N}$ |         Bitwise AND with a constant         | yes        | yes     |
| $\mathtt{\|}_\mathbb{N}$ |         Bitwise OR with a constant          | yes        | yes     |
|           $S$            |                    S-Box                    | no         | yes     |
|  $\boxplus_\mathbb{N}$   |          Modular integer addition           | no         | no      |
|  $\boxminus_\mathbb{N}$  |        Modular integer substraction         | no         | no      |
